# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.5](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/compare/v0.0.4...v0.0.5) (2021-09-28)


### 🐛 Bug Fixes

* corrige la copie de fichiers avec les fichiers cachés ([eaebbd1](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/eaebbd160f665ac3c3821f66e15dbf9eba6ad962))

### [0.0.4](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/compare/v0.0.3...v0.0.4) (2021-09-28)


### 🐛 Bug Fixes

* corrige versionrc + la récupération des fichiers à copier ([52a26e6](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/52a26e654d9ed6257be4e83ca49f752880da7dc5))

### [0.0.3](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/compare/v0.0.2...v0.0.3) (2021-09-28)


### ✨ Features

* ajout docker-compose + commande + package.json ([4299266](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/42992660c0ba6ee18e06a53dd0f1962ee1bde030))

### [0.0.2](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/compare/v0.0.1...v0.0.2) (2021-09-28)


### 📦 Build

* **version:** corrige le problème de version qui ne se met pas à jour sur le composer.json ([21664fb](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/21664fb99b93bd538bf9404764ba6d8d9df96c58))

### 0.0.1 (2021-09-19)


### ✨ Features

* ajout template Projet Symfony + README + phpunit ([f16d0ac](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/f16d0ac2cb11d49486e125137d3f7ed0cf229127))
* initialisation du skeleton-bundle ([55f7e26](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/55f7e26348980f697ce0291cbb9a091ddebe9353))


### 🏗 Chore

* tente la ci avec l'image officielle ([1dd7ddb](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/1dd7ddbb1e9f4da032ccad8f89053607904d2807))


### 🐛 Bug Fixes

* ajout variable docker auth pour pull l'image dans la CI ([4a3d65a](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/4a3d65a87e7da86f06fc5d49caa2bfe6ca8f5da0))
* corrige .versionrc + améliore gitlab-ci ([004b693](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/004b6938b058410459954dabfc82bd733a769c62))
* corrige le .versionrc ([1c00bcc](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/1c00bcc2e1e722e61b25bdf6a24911acdc2dcdf9))
* corrige le template composer.json-dist et lance le script avant l'install ([1852282](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/185228221ccf85585de786876c81621ff8072451))
* docker login dans le gitlab ci ([30ef5d2](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/30ef5d2565b5e3d589843151328b8b79776d0762))
* remet le script en post install + essaye de lancer un composer install ([0e2bf75](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/0e2bf75d34ff155af3ad643d5ca0c24ab797ef23))
* supprime le composer lock et package lock et .gitlab-ci ([3a5abff](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/3a5abff6b082e13fb216e78c79f42c8d801ee11c))
* tente le composer install sans "@" ([7364f5e](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/7364f5e2fc03052d2b858e7be30daf460446b8e9))


### 🚧 CI

* ajout numéro de version et gitlab-ci pour créer le changelog ([67bcb85](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/67bcb85fbeb7eb1968e86ab53ada0493d1b79c80))
* clone le projet pour le gitlab-ci pour repush ([107b4bd](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/107b4bd77e7dbb2df33b60472ac7c853742c070c))
* configure git avant le npm run release ([0e2056e](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/0e2056ed17a9d149daedc8ab96c21aa86c13e978))
* correction versionrc pour mettre à jour le composer.json ([88a6d89](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/88a6d89723b1268e3f640527f6e9b4bfc2493e34))
* corrige CI : dossier du git clone ([ee62f6a](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/ee62f6a2d7be2c55fe9cbfb82584e0eafb1baae6))
* corrige CI en ajoutant user.name et user.email ([85a3c37](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/85a3c376371867e34502554582b1ffdc15b9cb64))
* image node sans précision de version ([9ddb650](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/9ddb650b7a5c573ff490aed9c4b271bd2713a037))
* retente avec le dependency proxy ([184973f](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/184973f910a8b9198551a1d8694a0053195b458e))
* utilise la variable de dependency proxy ([95a2b57](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/95a2b570c2022685b9e9196e5aec13d933a4e8ba))
* utilise un personnal access token ([01ed323](https://gitlab.com/tonyschmitt/php-bundle/skeleton-bundle/commit/01ed3236e31a4cd76610c42a1a33bf499b19637a))
