#!/usr/bin/php
<?php

echo "====== Configuration du composer.json ======";
echo "\n";
echo "\n";

$projectName = readline('Project name (vendor-name/project-name) : ');
echo "\n";
$description = readline('Description : ');
echo "\n";
$vendorNamespace = readline('Vendor namespace (VendorName) : ');
echo "\n";
$projectNamespace = readline('Project namespace (Project) : ');
echo "\n";

$replaces = [
    "{{ projectname }}" => $projectName,
    "{{ description }}" => $description,
    "{{ vendornamespace }}" => $vendorNamespace,
    "{{ projectnamespace }}" => $projectNamespace,
];

function copyFiles(mixed $distfile, array $replaces): void
{
    $target = substr($distfile, 18, -5);

    echo "Creating clean file ($target) from dist ($distfile)...\n";
    copy($distfile, $target);

    echo "Replace variables to $target...\n";
    applyValues($target, $replaces);
}

foreach (glob("skeleton/template/*-dist") as $distfile) {
    copyFiles($distfile, $replaces);
}

foreach (glob("skeleton/template/.*-dist") as $distfile) {
    copyFiles($distfile, $replaces);
}

echo "Removing dist files\n";

// Then we drop the skel dir, as it contains skeleton stuff.
delTree("skeleton");
unlink("composer.lock");
unlink("package-lock.json");
unlink(".gitlab-ci.yml");
unlink("CHANGELOG.md");

// We could also remove the composer.phar that the zend skeleton has here,
// but a much better choice is to remove that one from our fork directly.

echo "\033[0;32mdist script done...\n";


/**
 * A method that will read a file, run a strtr to replace placeholders with
 * values from our replace array and write it back to the file.
 *
 * @param string $target the filename of the target
 * @param array $replaces the replaces to be applied to this target
 */
function applyValues($target, $replaces)
{
    file_put_contents(
        $target,
        strtr(
            file_get_contents($target),
            $replaces
        )
    );
}


/**
 * A simple recursive delTree method
 *
 * @param string $dir
 * @return bool
 */
function delTree($dir)
{
    $files = array_diff(scandir($dir), array('.', '..'));
    foreach ($files as $file) {
        (is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
    }
    return rmdir($dir);
}

exit(0);
