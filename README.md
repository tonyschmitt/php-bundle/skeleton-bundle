# Skeleton Bundle

## 🧐 A propos

Skeleton pour créer un bundle PHP.

## 🏁 Pour démarrer
### Prérequis

Outils nécessaires :
- docker

### Créer le bundle


Pour créer votre bundle il vous suffit de lancer la commande suivante via docker :

```shell
docker run --rm --interactive --tty --volume $PWD:/app composer create-project tonyschmitt/skeleton-bundle <nomProjet>
```
